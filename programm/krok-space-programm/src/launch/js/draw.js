var mainWidth = remote.getGlobal('screenWidth');
var mainHeight = remote.getGlobal('screenHeight');

var cnv = document.querySelector('canvas');
var ctx = cnv.getContext('2d');
set_t(1000, 100, 100, 0.1, 500); // MIFG Mend
var t1me = (new Date()).getTime();
var hgth = 1000;
var wdth = 200;
var k = 1 / (maximum_h() + 2 * hgth);
var time_speed = 1 / 10;
var mustExplose = true;

cnv.height = mainHeight;
cnv.width = mainWidth;

/**
 * Функция, которая криво отрисовывает ракету (крайне убого)
 *
 * @author ShchMax
 * @file launch
 * @function
 * @param {number} x Координата левого края ракеты
 * @param {number} y Координата нижнего края ракеты
 * @param {number} percent Доля заполненности топлива
 * @param {number} sizeX Ширина ракеты
 * @param {number} sizeY Высота ракеты
 */
function draw_rocket(x, y, percent, sizeX, sizeY) {
	x *= mainWidth;
	y *= mainHeight;
	sizeX *= mainWidth;
	sizeY *= mainHeight;
	y = mainHeight - y;
	ctx.fillStyle = "white";
	ctx.strokeStyle = "black";
	var y1 = Math.floor(sizeY * percent);
	ctx.fillRect(x, y - sizeY, sizeX, y1);
	ctx.strokeRect(x, y - sizeY, sizeX, sizeY);
}

/**
 * Функция, которая отричовывает огонь от сопла
 *
 * @author ShchMax
 * @file launch
 * @function
 * @param {number} x Координата левого края ракеты
 * @param {number} y Координата нижнего края ракеты
 * @param {number} sizeX Ширина ракеты
 * @param {number} sizeY Высота ракеты
 */
function draw_fire(x, y, sizeX, sizeY) {
	x *= mainWidth;
	y *= mainHeight;
	sizeX *= mainWidth / 2;
	sizeY *= mainHeight / 2;
	var R = sizeX;
	y = mainHeight - y;
	ctx.fillStyle = "orange";
	ctx.strokeStyle = "orange";
	ctx.beginPath();
	ctx.arc(x + sizeX, y + R, R, 0, Math.PI, 1);
	ctx.moveTo(x + sizeX - R, y + R);
	ctx.lineTo(x + sizeX, y + R + sizeY);
	ctx.lineTo(x + sizeX + R, y + R);
	ctx.fill();
}

/**
 * Функция, которая отрисовывает вызрыв ракеты
 *
 * @author ShchMax
 * @file launch
 * @function
 * @param {number} sizeY Высота ракеты
 */
function draw_explosive(sizeY) {
	ctx.fillStyle = "red";
	ctx.strokeStyle = "red";
	ctx.beginPath();
	ctx.arc(0.5 * mainWidth, mainHeight, sizeY * mainHeight * 2, 0, Math.PI, 1);
	ctx.fill();
}

/**
 * Функция, которая отрисовывает всё
 *
 * @author ShchMax
 * @file launch
 * @function
 */
function redraw() {
	ctx.fillStyle = "#004400";
	ctx.fillRect(0, 0, mainWidth, mainHeight);
	var now = (((new Date()).getTime() - t1me)) * time_speed;
	var x = 0.5 - (wdth * k / 2);
	var y = Math.max(h(now) * k, 0);
	var percent = mass_part(now);
	var sizeX = wdth * k;
	var sizeY = hgth * k;
	draw_rocket(x, y, percent, sizeX, sizeY);
	if (now <= max_t) {
		draw_fire(x, y, sizeX, sizeY * v(now) / max_v);
	}
	if (h(now) < 0 && mustExplose) {
		draw_explosive(sizeY);
	}
}

let redrawer = setInterval(() => {
	redraw();
}, 20);
