const remote = require('electron').remote;
const { ipcRenderer } = require('electron');

/**
 * Подключение скриптов без html
 *
 * @author ShchMax
 * @file launch
 * @function
 * @param {string} src Путь к подключаемому модулю
 * @param {function} callback Функция callback
 */
function loadScripts( src, callback ) {
	var script = document.createElement("SCRIPT"),
	head = document.getElementsByTagName( "head" )[ 0 ],
	error = false;
	script.type = "text/javascript";
	script.onload = script.onreadystatechange = function( e ){
		if ( ( !this.readyState || this.readyState == "loaded" || this.readyState == "complete" ) ) {
			if ( !error ) {
				removeListeners();
				callback( true );
			} else {
				callback( false );
			}
		}
	};
	script.onerror = function() {
		error = true;
		removeListeners();
		callback( false );
	}
	function errorHandle( msg, url, line ) {
		if ( url == src ) {
			error = true;
			removeListeners();
			callback( false );
		}
		return false;
	}
	function removeListeners() {
		script.onreadystatechange = script.onload = script.onerror = null;
		if ( window.removeEventListener ) {
			window.removeEventListener('error', errorHandle, false );
		} else {
			window.detachEvent("onerror", errorHandle );
		}
	}
	if ( window.addEventListener ) {
		window.addEventListener('error', errorHandle, false );
	} else {
		window.attachEvent("onerror", errorHandle );
	}
	script.src = src;
	head.appendChild( script );
}
loadScripts('../useable/js/jquery.js', function( status ){});

/**
 * Выход из программы
 *
 * @author ShchMax
 * @file launch
 * @function
 */
function exit_programm() {
    ipcRenderer.send('exit');
}
