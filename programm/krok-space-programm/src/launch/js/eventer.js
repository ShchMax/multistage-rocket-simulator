/**
 * Обработчик клавиш
 *
 * @author ShchMax
 * @file launch
 * @function
 * @param event Событие нажаитя клавиши
 */
document.addEventListener('keydown', function(event) {
	var p = event.key;
    if (p == "Escape") {
        exit_programm();
    }
});
