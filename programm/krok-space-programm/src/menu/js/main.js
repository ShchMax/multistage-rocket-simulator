const {ipcRenderer} = require('electron');

/**
 * Выход из программы
 *
 * @author ShchMax
 * @file menu
 * @function
 */
function exit_programm() {
    ipcRenderer.send('exit');
}

/**
 * Переход на запуск
 *
 * @author ShchMax
 * @file menu
 * @function
 */
function start_launch() {
    ipcRenderer.send('launch');
}
