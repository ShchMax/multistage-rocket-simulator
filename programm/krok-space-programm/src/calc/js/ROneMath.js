var M;
var I;
var F;
var G;
var max_t;
var max_h;
var max_v;
var Mend;

/**
 * Часть топлива, которая ещё в ракете
 *
 * @author ShchMax
 * @file calc
 * @function
 * @param {number} t Время от начала полёта
 */
function mass_part(t) {
    if (t > max_t) {
        return 0;
    }
    return ((M - F / I * t) / M - (Mend / M)) / (1 - (Mend / M));
}

/**
 * Ускорение от времени
 *
 * @author ShchMax
 * @file calc
 * @function
 * @param {number} t Время от начала полёта
 */
function a(t) {
    if (t > max_t) {
        return -G;
    }
    return I * F / (I * M - F * t) - G;
}

/**
 * Скорость от времени
 *
 * @author ShchMax
 * @file calc
 * @function
 * @param {number} t Время от начала полёта
 */
function v(t) {
    if (t > max_t) {
        t -= max_t;
        return I * Math.log(M / Mend) - G * t;
    }
    return I * Math.log(I * M / (I * M - F * t)) - G * t;
}

/**
 * Высота от времени
 *
 * @author ShchMax
 * @file calc
 * @function
 * @param {number} t Время от начала полёта
 */
function h(t) {
    if (t > max_t) {
        t -= max_t;
        return max_h + max_v * t - G * t * t / 2;
    }
    return I * t - (G * t * t) / 2 - (I * (I * M - F * t) * Math.log((I * M) / (I * M - F * t))) / F;
}

/**
 * Время полёта на двигателях
 *
 * @author ShchMax
 * @file calc
 * @function
 */
function get_t() {
    return (M - Mend) * I / F;
}

/**
 * Конечная высота полёта на двигателях
 *
 * @author ShchMax
 * @file calc
 * @function
 */
function get_h() {
    return h(max_t);
}

/**
 * Конечная скорость полёта на двиателях
 *
 * @author ShchMax
 * @file calc
 * @function
 */
function get_v() {
    return v(max_t);
}

/**
 * Функция, которая выставляет основные параметры ракеты
 *
 * @author ShchMax
 * @file calc
 * @function
 * @param {number} m Масса ракеты
 * @param {number} i Удельный импульс ракеты
 * @param {number} f Сила тяги ракеты
 * @param {number} g Ускорение свободного падения
 * @param {number} mend Конечная масса ракеты
 */
function set_t(m, i, f, g, mend) {
    M = m;
    I = i;
    F = f;
    G = g;
    Mend = mend;
    max_t = get_t();
    max_h = get_h();
    max_v = get_v();
}

/**
 * Максимальная высота полёта
 *
 * @author ShchMax
 * @file calc
 * @function
 */
function maximum_h() {
    return Math.max(h(max_t + max_v / G), max_h);
}
